use proc_macro::TokenStream;
use quote::{quote, ToTokens};

#[cfg(not(feature = "std"))]
use alloc::{
    collections::btree_map::BTreeMap,
    format,
    string::{String, ToString},
    vec,
    vec::Vec,
};

#[cfg(feature = "std")]
use std::{collections::BTreeMap, string::String};

/// Keyword that identifies the group of a flow control operation
const FLOWCONTROL_GROUP: &str = "FLOWCONTROL";

/// Generate a `describe()` function returning a textual representation of the operation manifest.
/// It will be expanded aside the function where the `describe` attribute is applied.
///
/// Requires some information passed in as argument, and reads the rest at runtime so that it can
/// access the Cargo.toml of the crate where this macro is expanded.
///
/// The following Cargo environment variables are used to fill the correspondent manifest entries:
///
/// * name: CARGO_PKG_NAME (to snake_case)
/// * description: CARGO_PKG_DESCRIPTION
/// * repository: CARGO_PKG_REPOSITORY
/// * licence: CARGO_PKG_LICENSE
///
/// # Arguments
///  * inputs: Ordered collection of type names representing the types of the accepted inputs
///  * output: Output type name
///  * groups: Collection of group names matching the Operation finality
///  * config: Map of configuration parameter names toward the collection of respective allowed
///    valeus
///  * features: Collection of features supported by the binary
///  * fn_item: The token stream of the original function since it will be present in the output
///
/// # Return
/// A token stream of the function that produces the manifest upon invocation aside of the original
/// function that was annotated
pub fn impl_describe_function(
    inputs: Vec<String>,
    output: String,
    groups: Vec<String>,
    config: BTreeMap<String, Vec<String>>,
    features: Vec<String>,
    fn_item: &dyn ToTokens,
) -> TokenStream {
    let inputs = inputs
        .iter()
        .map(|s| format!("\"{}\"", s))
        .collect::<Vec<String>>()
        .join(",");
    let config = config
        .iter()
        .map(|(key, values)| {
            let values = values
                .iter()
                .map(|val| format!("\"{}\"", val).clone())
                .collect::<Vec<String>>()
                .join(",");
            format!("\"{}\":[{}]", key, values).clone()
        })
        .collect::<Vec<String>>()
        .join(",");
    let groups = groups
        .iter()
        .map(|s| format!("\"{}\"", s))
        .collect::<Vec<String>>()
        .join(", ");
    let features = features
        .iter()
        .map(|s| format!("\"{}\"", s))
        .collect::<Vec<String>>()
        .join(", ");
    #[cfg(not(feature = "std"))]
    let gen = quote! {
        #fn_item

        /// Generated `describe()` function returning a textual representation of the Operation manifest data
        pub fn describe() -> String {
          use alloc::format;

          format!("{{\"name\":\"{}\",\"description\":\"{}\",\"inputs\":[{}],\"config\":{{{}}},\"groups\":[{}],\"output\":\"{}\",\"repository\":\"{}\",\"license\":\"{}\",\"features\":[{}]}}",
            env!("CARGO_PKG_NAME"), env!("CARGO_PKG_DESCRIPTION"), #inputs, #config, #groups, #output,
            env!("CARGO_PKG_REPOSITORY"), env!("CARGO_PKG_LICENSE"), #features
          )
        }
    };
    #[cfg(feature = "std")]
    let gen = quote! {
        #fn_item

        /// Generated `describe()` function returning a textual representation of the Operation manifest data
        pub fn describe() -> String {
          format!("{{\"name\":\"{}\",\"description\":\"{}\",\"inputs\":[{}],\"config\":{{{}}},\"groups\":[{}],\"output\":\"{}\",\"repository\":\"{}\",\"license\":\"{}\",\"features\":[{}]}}",
            env!("CARGO_PKG_NAME").to_lowercase().replace("-", "_"), env!("CARGO_PKG_DESCRIPTION"), #inputs, #config, #groups, #output,
            env!("CARGO_PKG_REPOSITORY"), env!("CARGO_PKG_LICENSE"), #features
          )
        }
    };
    gen.into()
}

pub fn impl_describe_function_for_flowcontrol(
    groups: Vec<String>,
    config: BTreeMap<String, Vec<String>>,
    features: Vec<String>,
    fn_item: &dyn ToTokens,
) -> TokenStream {
    let mut groups = groups.clone();
    if !groups.iter().any(|group| group == FLOWCONTROL_GROUP) {
        groups.insert(0, FLOWCONTROL_GROUP.to_string());
    }
    impl_describe_function(vec![], "".to_string(), groups, config, features, fn_item)
}
