#![cfg_attr(not(feature = "std"), no_std)]

#[cfg(not(feature = "std"))]
extern crate alloc;

mod functions;
mod types;

// Export functions
#[cfg(feature = "std")]
pub use functions::cli::*;
pub use functions::*;
// Export types
#[cfg(feature = "std")]
pub use types::cli::*;
pub use types::*;
// Export macros
#[cfg(feature = "anagolay")]
pub use an_operation_support_macro::*;

#[cfg(all(target_arch = "wasm32", feature = "js"))]
mod js;
#[cfg(all(target_arch = "wasm32", feature = "js"))]
pub use js::*;
